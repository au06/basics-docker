# Basic Docker Task

# To start the application  With Docker Compose

start node server 

    npm install
    node server.js

start mongodb and mongo-express

docker-compose up

<!-- mongo-express is accessible @ localhost:8080
mongo-express UI - create a new database "my-db"
ongo-express UI - create a new collection "users" in the database "my-db"-->

Access the nodejs application from browser 

    http://localhost:3000

#### To build a docker image from the application

    docker build -t my-app:1.0 .
    or 
    docker build .
